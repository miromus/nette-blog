<?php
// source: /var/www/app/config/config.neon 
// source: /var/www/app/config/config.local.neon 

class Container_0ed71bc89d extends Nette\DI\Container
{
	protected $meta = [
		'types' => [
			'Nette\Application\Application' => [1 => ['application.application']],
			'Nette\Application\IPresenterFactory' => [1 => ['application.presenterFactory']],
			'Nette\Application\LinkGenerator' => [1 => ['application.linkGenerator']],
			'Nette\Caching\IStorage' => [1 => ['cache.storage']],
			'Nette\Database\Connection' => [1 => ['database.default.connection']],
			'Nette\Database\IStructure' => [1 => ['database.default.structure']],
			'Nette\Database\Structure' => [1 => ['database.default.structure']],
			'Nette\Database\IConventions' => [1 => ['database.default.conventions']],
			'Nette\Database\Conventions\DiscoveredConventions' => [1 => ['database.default.conventions']],
			'Nette\Database\Context' => [1 => ['database.default.context']],
			'Nette\Http\RequestFactory' => [1 => ['http.requestFactory']],
			'Nette\Http\IRequest' => [1 => ['http.request']],
			'Nette\Http\Request' => [1 => ['http.request']],
			'Nette\Http\IResponse' => [1 => ['http.response']],
			'Nette\Http\Response' => [1 => ['http.response']],
			'Nette\Http\Context' => [1 => ['http.context']],
			'Nette\Bridges\ApplicationLatte\ILatteFactory' => [1 => ['latte.latteFactory']],
			'Nette\Application\UI\ITemplateFactory' => [1 => ['latte.templateFactory']],
			'Nette\Mail\IMailer' => [1 => ['mail.mailer']],
			'Nette\Application\IRouter' => [1 => ['routing.router']],
			'Nette\Security\IUserStorage' => [1 => ['security.userStorage']],
			'Nette\Security\User' => [1 => ['security.user']],
			'Nette\Security\IAuthorizator' => [1 => ['security.authorizator']],
			'Nette\Http\Session' => [1 => ['session.session']],
			'Tracy\ILogger' => [1 => ['tracy.logger']],
			'Tracy\BlueScreen' => [1 => ['tracy.blueScreen']],
			'Tracy\Bar' => [1 => ['tracy.bar']],
			'App\Model\DatabaseManager' => [1 => ['24_App_CoreModule_Model_ArticleManager', 'authenticator']],
			'App\CoreModule\Model\ArticleManager' => [1 => ['24_App_CoreModule_Model_ArticleManager']],
			'App\Forms\FormFactory' => [1 => ['25_App_Forms_FormFactory']],
			'App\Forms\SignInFormFactory' => [1 => ['26_App_Forms_SignInFormFactory']],
			'App\Forms\SignUpFormFactory' => [1 => ['27_App_Forms_SignUpFormFactory']],
			'Nette\Security\IAuthenticator' => [1 => ['authenticator']],
			'App\Model\UserManager' => [1 => ['authenticator']],
			'App\Presenters\BasePresenter' => [
				1 => [
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\Application\UI\Presenter' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\Application\UI\Control' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\Application\UI\Component' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\ComponentModel\Container' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\ComponentModel\Component' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\Application\UI\IRenderable' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\ComponentModel\IContainer' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\ComponentModel\IComponent' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\Application\UI\ISignalReceiver' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\Application\UI\IStatePersistent' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'ArrayAccess' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
				],
			],
			'Nette\Application\IPresenter' => [
				[
					'29_App_CoreModule_Presenters_ContactPresenter',
					'30_App_CoreModule_Presenters_ArticlePresenter',
					'application.1',
					'application.2',
					'application.3',
					'application.4',
				],
			],
			'App\CoreModule\Presenters\ContactPresenter' => [1 => ['29_App_CoreModule_Presenters_ContactPresenter']],
			'App\CoreModule\Presenters\ArticlePresenter' => [1 => ['30_App_CoreModule_Presenters_ArticlePresenter']],
			'App\CoreModule\Presenters\AdministrationPresenter' => [1 => ['application.1']],
			'App\Presenters\ErrorPresenter' => [1 => ['application.2']],
			'NetteModule\ErrorPresenter' => [1 => ['application.3']],
			'NetteModule\MicroPresenter' => [1 => ['application.4']],
			'Nette\DI\Container' => [1 => ['container']],
		],
		'services' => [
			'24_App_CoreModule_Model_ArticleManager' => 'App\CoreModule\Model\ArticleManager',
			'25_App_Forms_FormFactory' => 'App\Forms\FormFactory',
			'26_App_Forms_SignInFormFactory' => 'App\Forms\SignInFormFactory',
			'27_App_Forms_SignUpFormFactory' => 'App\Forms\SignUpFormFactory',
			'29_App_CoreModule_Presenters_ContactPresenter' => 'App\CoreModule\Presenters\ContactPresenter',
			'30_App_CoreModule_Presenters_ArticlePresenter' => 'App\CoreModule\Presenters\ArticlePresenter',
			'application.1' => 'App\CoreModule\Presenters\AdministrationPresenter',
			'application.2' => 'App\Presenters\ErrorPresenter',
			'application.3' => 'NetteModule\ErrorPresenter',
			'application.4' => 'NetteModule\MicroPresenter',
			'application.application' => 'Nette\Application\Application',
			'application.linkGenerator' => 'Nette\Application\LinkGenerator',
			'application.presenterFactory' => 'Nette\Application\IPresenterFactory',
			'authenticator' => 'App\Model\UserManager',
			'cache.storage' => 'Nette\Caching\IStorage',
			'container' => 'Nette\DI\Container',
			'database.default.connection' => 'Nette\Database\Connection',
			'database.default.context' => 'Nette\Database\Context',
			'database.default.conventions' => 'Nette\Database\Conventions\DiscoveredConventions',
			'database.default.structure' => 'Nette\Database\Structure',
			'http.context' => 'Nette\Http\Context',
			'http.request' => 'Nette\Http\Request',
			'http.requestFactory' => 'Nette\Http\RequestFactory',
			'http.response' => 'Nette\Http\Response',
			'latte.latteFactory' => 'Latte\Engine',
			'latte.templateFactory' => 'Nette\Application\UI\ITemplateFactory',
			'mail.mailer' => 'Nette\Mail\IMailer',
			'routing.router' => 'Nette\Application\IRouter',
			'security.authorizator' => 'Nette\Security\IAuthorizator',
			'security.user' => 'Nette\Security\User',
			'security.userStorage' => 'Nette\Security\IUserStorage',
			'session.session' => 'Nette\Http\Session',
			'tracy.bar' => 'Tracy\Bar',
			'tracy.blueScreen' => 'Tracy\BlueScreen',
			'tracy.logger' => 'Tracy\ILogger',
		],
		'tags' => [
			'inject' => [
				'29_App_CoreModule_Presenters_ContactPresenter' => true,
				'30_App_CoreModule_Presenters_ArticlePresenter' => true,
				'application.1' => true,
				'application.2' => true,
				'application.3' => true,
				'application.4' => true,
			],
			'nette.presenter' => [
				'29_App_CoreModule_Presenters_ContactPresenter' => 'App\CoreModule\Presenters\ContactPresenter',
				'30_App_CoreModule_Presenters_ArticlePresenter' => 'App\CoreModule\Presenters\ArticlePresenter',
				'application.1' => 'App\CoreModule\Presenters\AdministrationPresenter',
				'application.2' => 'App\Presenters\ErrorPresenter',
				'application.3' => 'NetteModule\ErrorPresenter',
				'application.4' => 'NetteModule\MicroPresenter',
			],
		],
		'aliases' => [
			'application' => 'application.application',
			'cacheStorage' => 'cache.storage',
			'database.default' => 'database.default.connection',
			'httpRequest' => 'http.request',
			'httpResponse' => 'http.response',
			'nette.authorizator' => 'security.authorizator',
			'nette.database.default' => 'database.default',
			'nette.database.default.context' => 'database.default.context',
			'nette.httpContext' => 'http.context',
			'nette.httpRequestFactory' => 'http.requestFactory',
			'nette.latteFactory' => 'latte.latteFactory',
			'nette.mailer' => 'mail.mailer',
			'nette.presenterFactory' => 'application.presenterFactory',
			'nette.templateFactory' => 'latte.templateFactory',
			'nette.userStorage' => 'security.userStorage',
			'router' => 'routing.router',
			'session' => 'session.session',
			'user' => 'security.user',
		],
	];


	public function __construct(array $params = [])
	{
		$this->parameters = $params;
		$this->parameters += [
			'appDir' => '/var/www/app',
			'wwwDir' => '/var/www/www',
			'debugMode' => true,
			'productionMode' => false,
			'consoleMode' => false,
			'tempDir' => '/var/www/app/../temp',
			'defaultArticleUrl' => 'uvod',
			'contactEmail' => 'admin@localhost.cz',
		];
	}


	public function createService__24_App_CoreModule_Model_ArticleManager(): App\CoreModule\Model\ArticleManager
	{
		$service = new App\CoreModule\Model\ArticleManager($this->getService('database.default.context'));
		return $service;
	}


	public function createService__25_App_Forms_FormFactory(): App\Forms\FormFactory
	{
		$service = new App\Forms\FormFactory;
		return $service;
	}


	public function createService__26_App_Forms_SignInFormFactory(): App\Forms\SignInFormFactory
	{
		$service = new App\Forms\SignInFormFactory($this->getService('25_App_Forms_FormFactory'), $this->getService('security.user'));
		return $service;
	}


	public function createService__27_App_Forms_SignUpFormFactory(): App\Forms\SignUpFormFactory
	{
		$service = new App\Forms\SignUpFormFactory($this->getService('25_App_Forms_FormFactory'), $this->getService('authenticator'));
		return $service;
	}


	public function createService__29_App_CoreModule_Presenters_ContactPresenter(): App\CoreModule\Presenters\ContactPresenter
	{
		$service = new App\CoreModule\Presenters\ContactPresenter('admin@localhost.cz', $this->getService('mail.mailer'));
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->injectFormFactory($this->getService('25_App_Forms_FormFactory'));
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createService__30_App_CoreModule_Presenters_ArticlePresenter(): App\CoreModule\Presenters\ArticlePresenter
	{
		$service = new App\CoreModule\Presenters\ArticlePresenter('uvod', $this->getService('24_App_CoreModule_Model_ArticleManager'));
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->injectFormFactory($this->getService('25_App_Forms_FormFactory'));
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__1(): App\CoreModule\Presenters\AdministrationPresenter
	{
		$service = new App\CoreModule\Presenters\AdministrationPresenter(
			$this->getService('26_App_Forms_SignInFormFactory'),
			$this->getService('27_App_Forms_SignUpFormFactory')
		);
		$service->injectPrimary(
			$this,
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response'),
			$this->getService('session.session'),
			$this->getService('security.user'),
			$this->getService('latte.templateFactory')
		);
		$service->injectFormFactory($this->getService('25_App_Forms_FormFactory'));
		$service->invalidLinkMode = 5;
		return $service;
	}


	public function createServiceApplication__2(): App\Presenters\ErrorPresenter
	{
		$service = new App\Presenters\ErrorPresenter($this->getService('tracy.logger'));
		return $service;
	}


	public function createServiceApplication__3(): NetteModule\ErrorPresenter
	{
		$service = new NetteModule\ErrorPresenter($this->getService('tracy.logger'));
		return $service;
	}


	public function createServiceApplication__4(): NetteModule\MicroPresenter
	{
		$service = new NetteModule\MicroPresenter($this, $this->getService('http.request'), $this->getService('routing.router'));
		return $service;
	}


	public function createServiceApplication__application(): Nette\Application\Application
	{
		$service = new Nette\Application\Application(
			$this->getService('application.presenterFactory'),
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('http.response')
		);
		$service->catchExceptions = true;
		$service->errorPresenter = 'Error';
		Nette\Bridges\ApplicationTracy\RoutingPanel::initializePanel($service);
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\ApplicationTracy\RoutingPanel(
			$this->getService('routing.router'),
			$this->getService('http.request'),
			$this->getService('application.presenterFactory')
		));
		return $service;
	}


	public function createServiceApplication__linkGenerator(): Nette\Application\LinkGenerator
	{
		$service = new Nette\Application\LinkGenerator(
			$this->getService('routing.router'),
			$this->getService('http.request')->getUrl(),
			$this->getService('application.presenterFactory')
		);
		return $service;
	}


	public function createServiceApplication__presenterFactory(): Nette\Application\IPresenterFactory
	{
		$service = new Nette\Application\PresenterFactory(new Nette\Bridges\ApplicationDI\PresenterFactoryCallback($this, 5, '/var/www/app/../temp/cache/Nette%5CBridges%5CApplicationDI%5CApplicationExtension'));
		$service->setMapping(['*' => 'App\*Module\Presenters\*Presenter']);
		return $service;
	}


	public function createServiceAuthenticator(): App\Model\UserManager
	{
		$service = new App\Model\UserManager($this->getService('database.default.context'));
		return $service;
	}


	public function createServiceCache__storage(): Nette\Caching\IStorage
	{
		$service = new Nette\Caching\Storages\FileStorage('/var/www/app/../temp/cache');
		return $service;
	}


	public function createServiceContainer(): Nette\DI\Container
	{
		return $this;
	}


	public function createServiceDatabase__default__connection(): Nette\Database\Connection
	{
		$service = new Nette\Database\Connection('mysql:host=127.0.0.1;dbname=mvc_db', 'root', 'root', ['lazy' => true]);
		$this->getService('tracy.blueScreen')->addPanel('Nette\Bridges\DatabaseTracy\ConnectionPanel::renderException');
		Nette\Database\Helpers::createDebugPanel($service, true, 'default');
		return $service;
	}


	public function createServiceDatabase__default__context(): Nette\Database\Context
	{
		$service = new Nette\Database\Context(
			$this->getService('database.default.connection'),
			$this->getService('database.default.structure'),
			$this->getService('database.default.conventions'),
			$this->getService('cache.storage')
		);
		return $service;
	}


	public function createServiceDatabase__default__conventions(): Nette\Database\Conventions\DiscoveredConventions
	{
		$service = new Nette\Database\Conventions\DiscoveredConventions($this->getService('database.default.structure'));
		return $service;
	}


	public function createServiceDatabase__default__structure(): Nette\Database\Structure
	{
		$service = new Nette\Database\Structure($this->getService('database.default.connection'), $this->getService('cache.storage'));
		return $service;
	}


	public function createServiceHttp__context(): Nette\Http\Context
	{
		$service = new Nette\Http\Context($this->getService('http.request'), $this->getService('http.response'));
		trigger_error('Service http.context is deprecated.', 16384);
		return $service;
	}


	public function createServiceHttp__request(): Nette\Http\Request
	{
		$service = $this->getService('http.requestFactory')->createHttpRequest();
		return $service;
	}


	public function createServiceHttp__requestFactory(): Nette\Http\RequestFactory
	{
		$service = new Nette\Http\RequestFactory;
		$service->setProxy([]);
		return $service;
	}


	public function createServiceHttp__response(): Nette\Http\Response
	{
		$service = new Nette\Http\Response;
		return $service;
	}


	public function createServiceLatte__latteFactory(): Nette\Bridges\ApplicationLatte\ILatteFactory
	{
		return new class ($this) implements Nette\Bridges\ApplicationLatte\ILatteFactory {
			private $container;


			public function __construct(Container_0ed71bc89d $container)
			{
				$this->container = $container;
			}


			public function create(): Latte\Engine
			{
				$service = new Latte\Engine;
				$service->setTempDirectory('/var/www/app/../temp/cache/latte');
				$service->setAutoRefresh(true);
				$service->setContentType('html');
				Nette\Utils\Html::$xhtml = false;
				return $service;
			}
		};
	}


	public function createServiceLatte__templateFactory(): Nette\Application\UI\ITemplateFactory
	{
		$service = new Nette\Bridges\ApplicationLatte\TemplateFactory(
			$this->getService('latte.latteFactory'),
			$this->getService('http.request'),
			$this->getService('security.user'),
			$this->getService('cache.storage'),
			null
		);
		return $service;
	}


	public function createServiceMail__mailer(): Nette\Mail\IMailer
	{
		$service = new Nette\Mail\SendmailMailer;
		return $service;
	}


	public function createServiceRouting__router(): Nette\Application\IRouter
	{
		$service = App\RouterFactory::createRouter();
		return $service;
	}


	public function createServiceSecurity__authorizator(): Nette\Security\IAuthorizator
	{
		$service = new Nette\Security\Permission;
		$service->addRole('guest', null);
		$service->addRole('member', ['guest']);
		$service->addRole('admin', null);
		$service->addResource('Core:Administration');
		$service->addResource('Core:Article');
		$service->addResource('Core:Contact');
		$service->allow('guest', 'Core:Administration', 'login');
		$service->allow('guest', 'Core:Administration', 'register');
		$service->allow('guest', 'Core:Article', 'default');
		$service->allow('guest', 'Core:Article', 'list');
		$service->allow('guest', 'Core:Contact');
		$service->allow('member', 'Core:Administration', 'default');
		$service->allow('member', 'Core:Administration', 'logout');
		$service->addResource('Error');
		$service->allow('admin');
		$service->allow('guest', 'Error');
		return $service;
	}


	public function createServiceSecurity__user(): Nette\Security\User
	{
		$service = new Nette\Security\User(
			$this->getService('security.userStorage'),
			$this->getService('authenticator'),
			$this->getService('security.authorizator')
		);
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\SecurityTracy\UserPanel($service));
		return $service;
	}


	public function createServiceSecurity__userStorage(): Nette\Security\IUserStorage
	{
		$service = new Nette\Http\UserStorage($this->getService('session.session'));
		return $service;
	}


	public function createServiceSession__session(): Nette\Http\Session
	{
		$service = new Nette\Http\Session($this->getService('http.request'), $this->getService('http.response'));
		$service->setExpiration('14 days');
		return $service;
	}


	public function createServiceTracy__bar(): Tracy\Bar
	{
		$service = Tracy\Debugger::getBar();
		return $service;
	}


	public function createServiceTracy__blueScreen(): Tracy\BlueScreen
	{
		$service = Tracy\Debugger::getBlueScreen();
		return $service;
	}


	public function createServiceTracy__logger(): Tracy\ILogger
	{
		$service = Tracy\Debugger::getLogger();
		return $service;
	}


	public function initialize()
	{
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\DITracy\ContainerPanel($this));
		Nette\Forms\Validator::$messages[Nette\Forms\Form::REQUIRED] = 'Povinné pole.';
		Nette\Forms\Validator::$messages[Nette\Forms\Form::EMAIL] = 'Neplatná emailová adresa.';
		$this->getService('http.response')->setHeader('X-Powered-By', 'Nette Framework');
		$this->getService('http.response')->setHeader('Content-Type', 'text/html; charset=utf-8');
		$this->getService('http.response')->setHeader('X-Frame-Options', 'SAMEORIGIN');
		$this->getService('session.session')->exists() && $this->getService('session.session')->start();
		Tracy\Debugger::$editorMapping = [];
		Tracy\Debugger::getLogger($this->getService('tracy.logger'))->mailer = [new Tracy\Bridges\Nette\MailSender($this->getService('mail.mailer'), null), 'send'];
		if ($tmp = $this->getByType("Nette\Http\Session", false)) { $tmp->start(); Tracy\Debugger::dispatch(); };
	}
}
