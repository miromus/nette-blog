<?php
// source: /var/www/app/CoreModule/templates/Article/list.latte

use Latte\Runtime as LR;

class Template4333ad6d22 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'description' => 'blockDescription',
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'title' => 'html',
		'description' => 'html',
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
		$this->renderBlock('description', get_defined_vars());
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['article'])) trigger_error('Variable $article overwritten in foreach on line 6');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockTitle($_args)
	{
		?>Výpis článků<?php
	}


	function blockDescription($_args)
	{
		?>Výpis všech článků.<?php
	}


	function blockContent($_args)
	{
		extract($_args);
?>
<table>
<?php
		$iterations = 0;
		foreach ($articles as $article) {
?>	<tr>
		<td>
			<h2><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Article:", [$article->url])) ?>"><?php
			echo LR\Filters::escapeHtmlText($article->title) /* line 8 */ ?></a></h2>
			<?php echo LR\Filters::escapeHtmlText($article->description) /* line 9 */ ?>

			<br>
<?php
			if ($user->isInRole('admin')) {
				?>				<a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("editor", [$article->url])) ?>">Editovat</a>
				<a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("remove", [$article->url])) ?>">Odstranit</a>
<?php
			}
?>
		</td>
	</tr>
<?php
			$iterations++;
		}
?>
</table>
<?php
	}

}
