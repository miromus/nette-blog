-- ----------------------------
-- Table structure for `article`
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
	`article_id`  int(11) NOT NULL                   AUTO_INCREMENT,
	`title`       varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
	`content`     text COLLATE utf8_czech_ci,
	`url`         varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
	`description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
	PRIMARY KEY (`article_id`),
	UNIQUE KEY `url` (`url`)
) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARSET = utf8 COLLATE = utf8_czech_ci;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', 'Úvod', '<p>Vítejte na našem webu!</p><p>Tento web je postaven na <strong>jednoduchém redakčním systému v Nette frameworku</strong>. Toto je úvodní článek, načtený z databáze.</p>', 'uvod', 'Úvodní článek na webu v Nette v PHP');
INSERT INTO `article` VALUES ('2', 'Stránka nebyla nalezena', '<p>Litujeme, ale požadovaná stránka nebyla nalezena. Zkontrolujte prosím URL adresu.</p>', 'chyba', 'Stránka nebyla nalezena.');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
	`user_id`  int(11)                                        NOT NULL AUTO_INCREMENT,
	`username` varchar(255) COLLATE utf8_czech_ci             NOT NULL,
	`password` varchar(60) COLLATE utf8_czech_ci              NOT NULL,
	`role`     enum ('member', 'admin') COLLATE utf8_czech_ci NOT NULL DEFAULT 'member',
	PRIMARY KEY (`user_id`),
	UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 DEFAULT CHARSET = utf8 COLLATE = utf8_czech_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '$2y$10$h8vmMU0yHJ4jFOpfxrZO0eIW3qgnRFXsdi4G9DKzXaHuo9OLPuPJu', 'admin');
INSERT INTO `user` VALUES ('2', 'test', '$2y$10$Re6SSHFjyr25eaddRBQHP.tvQ0nUr0EqUK05y12bGhgM.MzeHa5c6', 'member');
